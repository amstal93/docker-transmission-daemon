# Transmission-daemon

## Description

Docker image featuring configuration mounting and full volume setup, with additional features provided by the [alpine-base image](https://gitlab.com/dotslashme/docker-alpine-base/-/blob/main/README.md).

[Source](https://gitlab.com/dotslashme/docker-transmission-daemon) | [Issues](https://gitlab.com/dotslashme/docker-transmission-daemon/-/issues)

## Features

- Custom config in the form of a `settings.json` mounted as a volume.
- Complete volume setup availble, but can be used with a single volume as well.
- Healthcheck for RPC endpoint.

### Volumes

Only one volume is defined in this image:

- /opt/transmission (this is where transmission expects your settings.json to be)

### Healthcheck

There is a healthcheck set up for the default RPC url that transmission uses (http://localhost:9091/transmission/web). If you decide to customize this url, the healthcheck will no longer work as intended. If you use a customized url for your RPC connection, you should apply your own healthcheck paramaters through the `docker run` command or in your `docker-compose.yml` file.

### Environment variables

- PUID - Used to customize the UID that runs the transmission-daemon. This can be very useful in case you have multiple containers that need to work against a common shared folder.
- PGID - Used to customize the GID that runs the transmission-daemon. This can be very useful in case you have multiple containers that need to work against a common shared folder.


## Exposing the container

### Ports

In order to expose the container on the hosts network, you will need to map ports in your `docker run` command:

```shell
docker run -d --name transmission -p 9091:9091 dotslashme/transmission-daemon
```

or expose your port in your `docker-compose.yml`:

```yaml
...(omitted)
  transmission:
    image: dotslashme/transmission-daemon
    ports:
      - "9091:9091"
...(omitted)
```

If you are running your transmission behind a proxy that has access to the network of your transmission container, there is no need to expose the ports, since they are exposed internally to the proxy.
